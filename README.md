# RateRecords #

A Pyramid app with a basic record and RESTful service.

## Command Line ##

Run from the command line with a single argument for the file to process:

```
$ python records.py rockers.txt
SortBy.GENDER:
Harrie,Debbie,F,purple,07/01/1945
Jett,Joan,F,yellow,09/22/1958
Bonham,John,M,yellow,05/31/1948
Jones,John Paul,M,green,01/03/1946
Page,Jimmy,M,red,01/09/1944
Plant,Robert,M,blue,08/20/1948

SortBy.DOB:
Page,Jimmy,M,red,01/09/1944
Harrie,Debbie,F,purple,07/01/1945
Jones,John Paul,M,green,01/03/1946
Bonham,John,M,yellow,05/31/1948
Plant,Robert,M,blue,08/20/1948
Jett,Joan,F,yellow,09/22/1958

SortBy.NAME:
Plant,Robert,M,blue,08/20/1948
Page,Jimmy,M,red,01/09/1944
Jones,John Paul,M,green,01/03/1946
Jett,Joan,F,yellow,09/22/1958
Harrie,Debbie,F,purple,07/01/1945
Bonham,John,M,yellow,05/31/1948
```

Duplicate records are quietly dropped. Invalid strings are spat back as errors but processing continues.

## REST API ##

To run the service listening on port 6543 issue

```
$ pserve path/to/development.ini
```

Then use tool of choice to POST and GET from the service:

```
$ curl -X POST -H "Content-type: application/json" -d 'Jett, Joan, F, yellow, 09/22/1958' http://127.0.0.1:6543/records
$ curl -X POST -H "Content-type: application/json" -d 'Page, Jimmy, M, red, 01/09/1944' http://127.0.0.1:6543/records
$ curl http://127.0.0.1:6543/records/name
[{"lastName": "Page", "firstName": "Jimmy", "favoriteColor": "red", "dateOfBirth": "01/09/1944"}, {"lastName": "Jett", "firstName": "Joan", "favoriteColor": "yellow", "dateOfBirth": "09/22/1958"}]
```
