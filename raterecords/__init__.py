from pyramid.config import Configurator


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application for the Records app
    """
    config = Configurator(settings=settings)

    config.add_route('home', '/')
    config.add_route('add_record', '/records')
    config.add_route('by_gender', '/records/gender')
    config.add_route('by_birthdate', '/records/birthdate')
    config.add_route('by_name', '/records/name')

    config.scan()

    return config.make_wsgi_app()

