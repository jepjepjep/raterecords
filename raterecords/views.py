import pyramid.httpexceptions as exc
from pyramid.view import view_config
from raterecords import records

persons = records.PersonCollection()


@view_config(route_name='home', request_method='GET', renderer='json')
def get_records(request):
    return get_by_name(request)


@view_config(route_name='add_record', request_method='POST', renderer='json')
def add_record(request):
    try:
        s = request.body.decode('utf-8')
        return persons.add(records.Person(s))
    except ValueError as ve:
        raise exc.HTTPBadRequest


@view_config(route_name='by_gender', request_method='GET', renderer='json')
def get_by_gender(request):
    return persons.sort_by(records.PersonCollection.SortBy.GENDER)


@view_config(route_name='by_birthdate', request_method='GET', renderer='json')
def get_by_birthdate(request):
    return persons.sort_by(records.PersonCollection.SortBy.DOB)


@view_config(route_name='by_name', request_method='GET', renderer='json')
def get_by_name(request):
    return persons.sort_by(records.PersonCollection.SortBy.NAME)
