from datetime import datetime
from enum import Enum
from operator import attrgetter
import sys


class Person(object):
    """A person with five fields"""

    def __init__(self, s):
        """Construct a new person from string form. The only legal forms are
        single-line delimited strings with one of three delimiters. Raises a ValueError
        if the string can't be parsed correctly."""
        for delimiter in self.DELIMITERS:
            fields = s.split(delimiter)
            if len(fields) == 5:
                (self.last, self.first, self.sex, self.color, date_string) = fields
                if self.sex.strip() not in ['M', 'F']:
                    raise ValueError("Gender must be one of [F] or [M].")
                self.dob = datetime.strptime(date_string.strip(), "%m/%d/%Y")
                return
        raise ValueError("String is not of valid form.")

    DELIMITERS = [',', ' ', '|']

    def __str__(self):
        return ",".join([self.last_name, self.first_name, self.gender, self.favorite_color, self.date_of_birth])

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__dict__ == other.__dict__
        return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return not self.__eq__(other)
        return NotImplemented

    def __json__(self, request):
        return {'lastName': self.last_name,
                'firstName': self.first_name,
                'favoriteColor': self.favorite_color,
                'dateOfBirth': self.date_of_birth}
    @property
    def last_name(self):
        return self.last.strip()

    @property
    def first_name(self):
        return self.first.strip()

    @property
    def gender(self):
        return self.sex.strip()

    @property
    def favorite_color(self):
        return self.color.strip()

    @property
    def date_of_birth(self):
        return self.dob.strftime("%m/%d/%Y")


class PersonCollection:
    """A collection of person records, sortable"""

    def __init__(self):
        self.persons = []

    def add(self, p):
        """Adds person p to this collection. Doesn't add it if the person
        already exists. Return the person that was so added."""
        if not p in self.persons:
            self.persons.append(p)
        return p

    def sort_by(self, key):
        """Returns the list of persons sorted by the specified key"""
        if key == self.SortBy.GENDER:
            s = sorted(self.persons, key=attrgetter('last_name'))
            return sorted(s, key=attrgetter('gender'))
        elif key == self.SortBy.NAME:
            return sorted(self.persons, key=attrgetter('last_name'), reverse=True)
        elif key == self.SortBy.DOB:
            return sorted(self.persons, key=attrgetter('dob'))
        raise ValueError("Must be a valid sort-by key.")

    class SortBy(Enum):
        GENDER = 1
        DOB = 2
        NAME = 3


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print('Usage: records.py [filename]')
        sys.exit(1)

    # Read in the person records
    filename = sys.argv[1]
    persons = PersonCollection()
    with open(filename, 'r') as infile:
        for line in infile:
            try:
                persons.add(Person(line))
            except ValueError as ve:
                print('Skipping [%s] because %s' % (line.strip(), str(ve)))


    def run(collection, key):
        print('%s:' % key)
        for p in collection.sort_by(key):
            print(p)
        print('')


    run(persons, PersonCollection.SortBy.GENDER)
    run(persons, PersonCollection.SortBy.DOB)
    run(persons, PersonCollection.SortBy.NAME)
