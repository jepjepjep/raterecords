import json
import unittest

from raterecords import records


class PersonTests(unittest.TestCase):
    def test_valid_constructor(self):
        s = ['last', 'first', 'M', 'blue', '12/31/2014']
        for delimiter in records.Person.DELIMITERS:
            p = records.Person(delimiter.join(s))
            self.assertEqual('last', p.last_name)
            self.assertEqual('first', p.first_name)
            self.assertEqual('M', p.gender)
            self.assertEqual('blue', p.favorite_color)
            self.assertEqual('12/31/2014', p.date_of_birth)

    def test_invalid_strings(self):
        for s in ['',
                  'last:first:F:red:12/31/2014',
                  'last,first,M,blue,',
                  'last,first,M,blue,12/31/2014,fred',
                  ',,,,'
                  'last,first,x,red,12/31/2014',
                  'last,first,M,red,12/31']:
            try:
                p = records.Person(s)
                self.fail('[%s] passed but should have failed.' % s)
            except ValueError:
                pass


class PersonCollectionTests(unittest.TestCase):
    def setUp(self):
        self.pc = records.PersonCollection()
        for rocker in ROCKERS:
            self.pc.add(records.Person(rocker))

    def test_basic_collection(self):
        self.assertEquals(6, len(self.pc.persons))
        jimmy = records.Person("Page, Jimmy, M, red, 01/09/1944")
        self.assertTrue(jimmy in self.pc.persons)

    def test_do_not_add_twice(self):
        self.pc.add(records.Person("Page, Jimmy, M, red, 01/09/1944"))
        self.assertEquals(6, len(self.pc.persons))

    def test_sort_by_gender(self):
        ordered = self.pc.sort_by(records.PersonCollection.SortBy.GENDER)
        self.assertEquals(6, len(self.pc.persons))
        self.assertEquals("Harrie", ordered[0].last_name)
        self.assertEquals("Plant", ordered[5].last_name)

    def test_sort_by_dob(self):
        ordered = self.pc.sort_by(records.PersonCollection.SortBy.DOB)
        self.assertEquals(6, len(self.pc.persons))
        self.assertEquals("Page", ordered[0].last_name)
        self.assertEquals("Jett", ordered[5].last_name)

    def test_sort_by_last_name(self):
        ordered = self.pc.sort_by(records.PersonCollection.SortBy.NAME)
        self.assertEquals(6, len(self.pc.persons))
        self.assertEquals("Plant", ordered[0].last_name)
        self.assertEquals("Bonham", ordered[5].last_name)


class FunctionalTests(unittest.TestCase):
    def setUp(self):
        from raterecords import main
        app = main({})
        from webtest import TestApp
        self.testapp = TestApp(app)

    def test_empty_get(self):
        res = self.testapp.get('/')
        self.assertEquals(200, res.status_code)
        self.assertEquals(0, len(res.json))

    def test_single_record_post(self):
        res = self.testapp.post('/records', ROCKERS[0])
        self.assertEquals(200, res.status_code)

    def test_bad_record_post(self):
        res = self.testapp.post('/records', 'last,first,M,red,12/31', status=400)
        self.assertEquals(400, res.status_code)

    def add_and_fetch(self, endpoint):
        """POSTs all the person records, then fetches them as a list from the specified endpoint.
        Verifies basic operation but not the order of the records, which are returned."""
        for rocker in ROCKERS:
            self.testapp.post('/records', rocker)
        res = self.testapp.get(endpoint)
        self.assertEquals(200, res.status_code)
        self.assertEquals(len(ROCKERS), len(res.json))
        return res.json

    def test_get_by_gender(self):
        rockers = self.add_and_fetch('/records/gender')
        self.assertEquals("Harrie", rockers[0]['lastName'])
        self.assertEquals("Plant", rockers[5]['lastName'])

    def test_get_by_date_of_birth(self):
        rockers = self.add_and_fetch('/records/birthdate')
        self.assertEquals("Page", rockers[0]['lastName'])
        self.assertEquals("Jett", rockers[5]['lastName'])

    def test_get_by_name(self):
        rockers = self.add_and_fetch('/records/name')
        self.assertEquals("Plant", rockers[0]['lastName'])
        self.assertEquals("Bonham", rockers[5]['lastName'])

ROCKERS = [
    'Jett, Joan, F, yellow, 09/22/1958',
    'Plant, Robert, M, blue, 08/20/1948',
    'Bonham, John, M, yellow, 05/31/1948',
    'Page, Jimmy, M, red, 01/09/1944',
    'Jones, John Paul, M, green, 01/03/1946',
    'Harrie, Debbie, F, purple, 07/01/1945'
]

if __name__ == '__main__':
    unittest.main()
